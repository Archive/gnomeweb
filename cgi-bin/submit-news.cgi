#!/usr/bin/perl

use CGI; use Mail::Send;

$address = "news\@gnome.org";

$q = new CGI;

$name = $q->param('yourname');
$email = $q->param('email');
$url = $q->param('url');
$title = $q->param('title');
$comments = $q->param('comments');

$subject = "[GNOME NEWS] " . $title;

$headers = {Subject => $subject, From => $email};

$mailer = new Mail::Mailer 'sendmail', $address;
$mailer->open ($headers);

print $mailer <<EOF;
Name: $name
Email: $email
URL: $url
Subject: $title

Comments:
$comments
EOF
$mailer->close;

print $q->redirect('/news/submit-thanks.shtml');
