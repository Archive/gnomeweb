#!/usr/bin/perl
# Author:   Joseph Wilhelm
# Purpose:  Feedback form for the GNOME page
# Date:     08-21-99

# email1.cgi

print "Content-type: text/html\n\n";

if ($ENV{'REQUEST_METHOD'} != 'POST')
{
    print <<"HTML";
    <html>
    <head>
    <title>Sorry!</title>
    </head>
    <body>
    This script only accepts the POST method so far!
    </body>
    </html>
HTML
    exit();
}

read(STDIN, $form_info, $ENV{'CONTENT_LENGTH'});
@args = split(/&/, $form_info);

foreach $arg (@args)
{
    ($key, $value) = split(/=/, $arg);
    $value =~ tr/+/ /;
    $value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
    $form{$key} = $value;
}

chop($date = 'date');

&not_complete unless $form{'Name'};
&not_complete unless $form{'Reply-to'};
&not_complete unless $form{'Content'};

if ($form{'Name'} =~ /[!@#\$%^&*()+=:\/]/)
{
    &not_name;
}

if ($form{'Reply-to'} !~ /@/)
{
    &not_reply;
}

open(MAIL, "|/usr/lib/sendmail -t");
print MAIL "To: tarken\@ior.com\n";
print MAIL "From: $form{'Reply-to'} <$form{'Name'}>\n";
print MAIL "Subject: www.gnome.org FEEDBACK\n\n";
print MAIL "$form{'Content'}\n";
close(MAIL);
print <<"HTML";
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<HTML>
<HEAD>
 <TITLE>Feedback Submitted</TITLE>
</HEAD>

<BODY BGCOLOR="#FFFFFF" TEXT="#000000" BACKGROUND="/images/bg-really-light.jpg"
      LINK="#0000BB" VLINK="#551A8B" ALINK="#FF0000" >
  <table border=0 cellpadding="0" cellspacing="3" width="100%">
  <tr><td>
    <table border=0 cellpadding="0" cellspacing="3" width="100%">
    <tr><td bgcolor="#777777">
      <table  border="0" cellpadding="0" cellspacing="2" width="100%" align="center">
      <tr><td bgcolor="#ffffff" colspan="0" width="100%" >
        <table cellspacing="0" width="100%" >
        <tr><td>
          <IMG SRC="/images/header-inside.gif" ALT="GNOME Developer"  BORDER=0
           usemap="#inside">

          <MAP name="inside">
             <area coords="0,0,225,49"       href="/">
             <area coords="241,10,337,21"    href="http://www.gnome.org">
             <area coords="241,29,310,39"    href="http://www.gnome.org/ftpmirrors.shtml">
             <area coords="357,10,383,21"    href="http://cvs.gnome.org/lxr">
             <area coords="357,28,397,38"    href="http://cvs.gnome.org/bonsai">
             <area coords="415,10,490,21"    href="http://bugs.gnome.org">
             <area coords="415,28,501,38"    href="http://www.gnome.org/applist">
          </MAP>
        </td></tr>
        </table>
      </td></tr>
      </table>
    </td></tr>
    </table>
  </td></tr>
  <tr><td rowspan="0" >
    <table border=0 cellpadding=0 cellspacing="3" width="100%" height="100%">
    <tr><td valign="top" align="left" width="20%">
      <table border=0 cellpadding=0 cellspacing=0 width="100%" height="100%">
      <tr><td valign="top">
        <table border=0 cellpadding="0" cellspacing="0" width="100%" height="100%">
        <tr><td bgcolor="#777777" valign="top">
          <table  border="0" cellpadding="5" cellspacing="2" width="100%" align="center" height="100%">
          <tr><td  valign="top" bgcolor="#ffffff" nowrap>
            <small>
            Home<br>
<a href="about/">About GNOME</a><br>
<a href="news/">News</a><br>
<a href="docs/">Documentation</a><br>
<a href="screenshots/">Screenshots</a><br>
<a href="appsites/">Application sites</a><br>
<a href="mailing-lists/">Mail Lists</a><br>
<a href="http://www.developer.gnome.org/">developers</a><br>
<a href="links.html">External Links</a><br>
<a href="thanks.html">Thank You</a><br>

            </small>
          </td></tr>
          </table>
        </td></tr>
        </table>
      </td></tr>
      </table>
    </td><td valign="top" >
      <table border=0 cellpadding="0" cellspacing="0" width="100%">
      <tr><td bgcolor="#777777" valign="top" >
        <table  border="0" cellpadding="5" cellspacing="2" width="100%" align="center">
        <tr><td valign="top" bgcolor="#ffffff">

    Your feedback has been sent to webmaster@gnome.org<br>
    <br>
    <a href="index.html">Return to the main page</a>
        </td></tr>
        </table>
      </td></tr>
      </table>
    <tr><td></td><td>
<center>
<p>
<font size=2><A href="/">Home</A> | <A href="/news">News</A> | <A href="/arch">Architecture</A> | <A href="/doc">Documentation</A> | <A href="/tools">Tools</A> | <A href="/helping">Getting Involved</A> | <A href="/links">External Resources</A> | <A href="/sitemap.html">Sitemap</A></font><p>
 <H6><font size=1><a href="mailto:webmaster\@gnome.org">webmaster\@gnome.org</a></font></H6>
 <H6>Website hosted by <a href="http://www.redhat.com/">Red Hat, Inc.</a></H6>
</center>


    </td></tr>
    </td></tr>
    </table>
  </td></tr>
  <tr><td>
  </td></tr>
  </table>
</BODY>
</HTML>
HTML
exit();

sub not_complete {
    &error_page();
    exit();
}

sub not_name {
    &error_page(" \
<H1> Name Incorrect</H1> \
I'm sorry, but the name you entered is not correct. Please try \
entering a proper name, first and last. \
<br> \
Thank you<P> \
<HR>");
    exit();
}

sub not_reply {
    &error_page(" \
<H1> Email Address Incorrect</H1> \
I'm sorry, but the email address you entered is not correct. Please try \
entering a proper email address like so: user\@somewhere.com. \
<br> \
Thank you<P> \
<HR>");
    exit();
}

sub error_page {
    $msg = $_[0] || "Please fill in the form completely before pressing the submit button.";
    print <<"HTML";
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
    <HTML>
    <HEAD>
     <TITLE>GNOME Feedback Form - Error</TITLE>
    </HEAD>
    
    <BODY BGCOLOR="#FFFFFF" TEXT="#000000" BACKGROUND="/images/bg-really-light.jpg"
          LINK="#0000BB" VLINK="#551A8B" ALINK="#FF0000" >
      <table border=0 cellpadding="0" cellspacing="3" width="100%">
      <tr><td>
        <table border=0 cellpadding="0" cellspacing="3" width="100%">
        <tr><td bgcolor="#777777">
          <table  border="0" cellpadding="0" cellspacing="2" width="100%" align="center">
          <tr><td bgcolor="#ffffff" colspan="0" width="100%" >
            <table cellspacing="0" width="100%" >
            <tr><td>
              <IMG SRC="/images/header-inside.gif" ALT="GNOME Developer"  BORDER=0
               usemap="#inside">
    
              <MAP name="inside">
                 <area coords="0,0,225,49"       href="/">
                 <area coords="241,10,337,21"    href="http://www.gnome.org">
                 <area coords="241,29,310,39"    href="http://www.gnome.org/ftpmirrors.shtml">
                 <area coords="357,10,383,21"    href="http://cvs.gnome.org/lxr">
                 <area coords="357,28,397,38"    href="http://cvs.gnome.org/bonsai">
                 <area coords="415,10,490,21"    href="http://bugs.gnome.org">
                 <area coords="415,28,501,38"    href="http://www.gnome.org/applist">
              </MAP>
            </td></tr>
            </table>
          </td></tr>
          </table>
        </td></tr>
        </table>
      </td></tr>
      <tr><td rowspan="0" >
        <table border=0 cellpadding=0 cellspacing="3" width="100%" height="100%">
        <tr><td valign="top" align="left" width="20%">
          <table border=0 cellpadding=0 cellspacing=0 width="100%" height="100%">
          <tr><td valign="top">
            <table border=0 cellpadding="0" cellspacing="0" width="100%" height="100%">
            <tr><td bgcolor="#777777" valign="top">
              <table  border="0" cellpadding="5" cellspacing="2" width="100%" align="center" height="100%">
              <tr><td  valign="top" bgcolor="#ffffff" nowrap>
                <small>
                Home<br>
    <a href="about/">About GNOME</a><br>
    <a href="news/">News</a><br>
    <a href="docs/">Documentation</a><br>
    <a href="screenshots/">Screenshots</a><br>
    <a href="mailing-lists/">Mail Lists</a><br>
    <a href="http://www.developer.gnome.org/">developers</a><br>
    <a href="links.html">External Links</a><br>
    <a href="thanks.html">Thank You</a><br>
    
                </small>
              </td></tr>
              </table>
            </td></tr>
            </table>
          </td></tr>
          </table>
        </td><td valign="top" >
          <table border=0 cellpadding="0" cellspacing="0" width="100%">
          <tr><td bgcolor="#777777" valign="top" >
            <table  border="0" cellpadding="5" cellspacing="2" width="100%" align="center">
            <tr><td valign="top" bgcolor="#ffffff">
              <!--#set var="last_modified" value="$Date: 1999/09/17 14:23:29 $" -->
$msg
        <form method="POST" action="email1.cgi">
            <strong>Your name:</strong><br>
            <input type="text" length="20" name="Name" value="$form{'Name'}"><br>
            <strong>Your email address:</strong><br>
            <input type="text" length="15" name="Reply-to" value="$form{'Reply-to'}"><br>
            <strong>Type your message here:</strong><br>
            <textarea rows="20" cols="50" name="Content">
$form{'Content'}
            </textarea><br>
            <input type="submit"><input type="reset">
        </form>

        </td></tr>
        </table>
      </td></tr>
      </table>
    <tr><td></td><td>
<center>
<p>
<font size=2><A href="/">Home</A> | <A href="/news">News</A> | <A href="/arch">Architecture</A> | <A href="/doc">Documentation</A> | <A href="/tools">Tools</A> | <A href="/helping">Getting Involved</A> | <A href="/links">External Resources</A> | <A href="/sitemap.html">Sitemap</A></font><p>
 <H6><font size=1><a href="mailto:webmaster\@gnome.org">webmaster\@gnome.org</a></font></H6>
 <H6>Website hosted by <a href="http://www.redhat.com/">Red Hat, Inc.</a></H6>
</center>


    </td></tr>
    </td></tr>
    </table>
  </td></tr>
  <tr><td>
  </td></tr>
  </table>
HTML
exit();
}
