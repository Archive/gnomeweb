#!/usr/bin/perl -w

$infile = shift @ARGV;

# won't work with paths containing .. or .
if ($infile =~ /\.tmpl$/) {
    $basename = $infile;
    $basename =~ s/\.tmpl$//;
    print $basename;
    $outbase = $basename;
}
else {
    $outbase = $infile;
}

$outfile_txt = "$outbase.txt";
$outfile_html = "$outbase.html";

@items = ();

open (IN, "$infile") || die ("Couldn't open $infile");

# Scan for TOC
while (<IN>) {
    if ($_ =~ /^\@item(.*)/) {
	$item = $1;
	chomp $item;
	push (@items, $item);
    }
}
close (IN);

open (IN, "$infile") || die ("Couldn't open $infile a second time");
open (OUT_TXT, ">$outfile_txt") || die ("Couln't open $outfile_txt for writing");

$count = 1;

while (<IN>) {
    if ($_ =~ /^\@item(.*)/) {
	$item = $1;
	chomp $item;
	print OUT_TXT "==============================================================\n\n";
	print OUT_TXT " $count) $item\n\n";
	print OUT_TXT "--------------------------------------------------------------\n\n";
	$count = $count + 1;
    }
    elsif ($_ =~ /^\@date(.*)/) {
	$date = $1;
	chomp $date;
	print OUT_TXT "This is the GNOME Summary for $date.\n\n";
	print OUT_TXT "=============================================================\n";
	print OUT_TXT "  Table of Contents\n";
	print OUT_TXT "-------------------------------------------------------------\n\n";
	# C programmers using perl, sigh
	$i = 0;
	while ($i <= $#items)
	{
	    $n = $i + 1;
	    print OUT_TXT " $n) ";
	    print OUT_TXT $items[$i];
	    print OUT_TXT "\n";

	    $i = $i + 1;
	}	
        print "\n";
    }
    else {
	print OUT_TXT $_;
    }
}
close (IN);
close (OUT_TXT);

open (IN, "$infile") || die ("Couldn't open $infile a second time");
open (OUT_HTML, ">$outfile_html") || die ("Couln't open $outfile_html for writing");

$count = 1;

while (<IN>) {
    if ($_ =~ /^\@item(.*)/) {
	$item = $1;
	chomp $item;
	print OUT_HTML "<p><h2><b>$count)</b> $item</h2>\n<p>\n";
	$count = $count + 1;
    }
    elsif ($_ =~ /^\@date(.*)/) {
	$date = $1;
	chomp $date;
	print OUT_HTML "<p>This is the GNOME Summary for $date.\n\n";
	print OUT_HTML "<p><h2>Table of Contents</h2><p>\n";

	# C programmers using perl, sigh
	$i = 0;
	while ($i <= $#items)
	{
	    $n = $i + 1;
	    print OUT_HTML "<h3>$n) ";
	    print OUT_HTML $items[$i];
	    print OUT_HTML "</h3>";

	    $i = $i + 1;
	}
	print OUT_HTML "<p>\n";
    }
    elsif ($_ =~ /http:\/\//) {
	$url = $_;
	$url =~ s/\s//g;
	print OUT_HTML "<p><blockquote><a href=\"$url\">$url</a></blockquote><p>";
    }
    elsif ($_ =~ /^\s$/) {
	print OUT_HTML "<p>";
    }
    else {
	print OUT_HTML "<br>";
	print OUT_HTML $_;
    }
}
close (IN);
close (OUT_HTML);








