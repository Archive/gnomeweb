use FileHandle;


my %months = ("Jan" => 0,
              "Feb" => 31,
				  "Mar" => 59,
				  "Apr" => 90,
				  "May" => 120,
				  "Jun" => 151,
              "Jul" => 181,
				  "Aug" => 212,
				  "Sep" => 243,
				  "Oct" => 273,
				  "Nov" => 304,
				  "Dec" => 334);
my $htmldir;
my $basedir;
my $templatepfx;
my $contentdir;
my $templatesdir;
my $treefile = "DIR";
my $varfile = "VARS";
my $extension = ".html";
my %variables;
my @main_index;

sub build_navigation  {
	my ($navref, $parentref, $childref, $title) = @_;
	my ($endstr, $startstr);

	foreach (@$parentref) {
		m/(.+):(.+)/;
		push (@$navref, "$startstr<A href=\"$1\">$2</A><BR>\n");
		$startstr .= "&nbsp;&nbsp;&nbsp;";
	}

	push (@$navref, "$startstr$title<BR>\n");
	$startstr .= "&nbsp;&nbsp;&nbsp;";

	foreach (@$childref) {
		m/(.+):(.+)/;
		push (@$navref, "$startstr<A href=\"$1\">$2</A><BR>\n");
	}
	push (@$navref, "$endstr\n");
}

sub get_url_level ($) {
	my ($data) = @_;
	my ($update, $dir, $url, $title);

	$data =~ m,(UPDATED|NEW)?(?:(.*);)?(.*):(.*),;
	($update, $dir, $url, $title) = ($1, $2, $3, $4);
	if ($url =~ m/^(http|ftp):/) {
		my @tmp = $dir =~ m,/,g;
		return $#tmp + 1;
	} else {
		#remove starting and trailing /
		$url =~ s,^/,,;
		$url =~ s,/$,,;
		my @tmp = $url =~ m,.*/,g; # count /'s
		return $#tmp + 1;
	}
}

sub create_index_side ($$) {
	my ($sideref, $fileref) = @_;

	foreach $line (@$sideref) {
		my ($update, $dir, $url, $title);
		my ($level);
		my ($str);

		$line =~ m,(UPDATED|NEW)?(?:(.*);)?(.*):(.*),;
		($update, $dir, $url, $title) = ($1, $2, $3, $4);
		$level = get_url_level ($line);
		$url =~ s,^/,,;
		if ($level == 0) {
			$str = "&nbsp;&nbsp;&nbsp;" x $level . "<A class=lvl$level href=\"$url\"><font size=+1>$title</font></A>";
		} else {
			$str = "&nbsp;&nbsp;&nbsp;" x $level . "<A class=lvl$level href=\"$url\">$title</A>";
		}
		if ($update eq "NEW") {
			$str .= "<IMG src=\"/images/new.gif\" alt=\"[NEW]\">"
		} elsif ($update eq "UPDATE") {
			$str .= "<IMG src=\"/images/update.gif\" alt=\"[UPDATED]\">"
		}
		$str .= "<BR>\n";
		push (@$fileref, $str);
	}
}

sub build_main_index ($) {
	my ($fileref) = @_;
	my ($count, $i, $j);
	my ($level, $url);
	my (@one, @two);

	$count = $#main_index;
	$i = int ($count / 2) + 1;
	$j = $i + 1;

	while ((get_url_level ($main_index[$i]) != 0)
	    && (get_url_level ($main_index[$j]) != 0)) {

		$i--;
		$j++;
	}
	if (get_url_level ($main_index[$j]) == 0) {
		@one = @main_index[0..$j - 1];
		@two = @main_index[$j..$#main_index];
	} else {
		@one = @main_index[0..$i - 1];
		@two = @main_index[$i..$#main_index];
	}

	push (@$fileref, "<TABLE width=100%><TR><TD>\n");
	create_index_side (\@one, $fileref);
	push (@$fileref, "</TD><TD valign=top class=mainindex>\n");
	create_index_side (\@two, $fileref);
	push (@$fileref, "</TD></TR></TABLE>\n");
}

sub subst_var ($) {
	my ($var) = @_;
	my ($value);

	$value = $variables{$var};
	if ($value =~ /(.+)::(\".*\")/) {
		$value = "<A href=$2>$1</A>";
	}
	return $value;
}

sub parse_line ($$$$) {
	my ($line, $navref, $fileref, $varsref) = @_;

	if ($line =~ /^\s*\@\@MAININDEX\@\@\s*$/) {
		build_main_index($fileref);
		next;
	}
	if ($line =~ /^\s*\@\@NAVIGATION\@\@\s*$/) {
		push (@$fileref, @$navref);
		next;
	}
	while ($line =~ /\@\@(\w+)\@\@/) {
		my $var = $1;
		my $value;

		$value = $$varsref{$var};
		if (!$value) {
			$value = &subst_var ($var);
		}
		$line =~s/\@\@$var\@\@/$value/;
	}
	push (@$fileref, $line);
}

sub fill_page ($$$$$$) {
	my ($path, $file, $title, $linktitle, $parentref, $childref) = @_;
	my (@file, @content, %localvars);
	my (@navigation, $localtitle);
	my ($content, $output);

	# only use the last element of the title
	$localtitle = $linktitle;
	if ($localtitle =~ /:/) {
		$localtitle =~ s/.*:\s*//;
	}

	# replace .html extension with the content extension
	$content = $file;
	$content =~ s/\.html/$extension/;

	# create the full input and output path
	if ($path ne "") {
		$content = "$contentdir/$path/$content";
		$output = "$htmldir/$path";
	} else {
		$content = "$contentdir/$content";
		$output = "$htmldir";
	}
	# check if the output directory exists
	if (!(-d $output)) {
		mkdir ($output, 0777);
	}

	$localvars{"TITLE"} = $title;
	$localvars{"LINKTITLE"} = $linktitle;
	$localvars{"templatepfx"} = $templatepfx;

	# read in the content
	$output .= "/$file";
	open (CONTENT, "$content");
	# add each line
	foreach (<CONTENT>) {
		my $line = $_;
		# check for variables defined in the content
		if ($line =~ /^\s*<!--\s*#set var=\"(\w+)\"\s+value=\"(.*)\"\s*-->\s*/) {
			my ($var, $value) = ($1, $2);
			$localvars{$var} = $value;
		} else {
			push (@content, $line);
		}
	}
	close (CONTENT);
	# create the navigation table for this node
	&build_navigation (\@navigation, $parentref, $childref, $localvars{"LINKTITLE"});

	# add the top template
	open (TMPL, "$templatesdir/" . $localvars{"templatepfx"} . "top.tmpl");
	unshift (@content, <TMPL>);
	# add the bottom template
	open (TMPL, "$templatesdir/" . $localvars{"templatepfx"} . "bottom.tmpl");
	push (@content, <TMPL>);

	foreach (@content) {
		my $line = $_;
		&parse_line ($line, \@navigation, \@file, \%localvars);
	}

	# write away the output file
	open (OUTFILE, ">$output");
	print OUTFILE (@file);
	close (OUTFILE);
}

sub read_dates {
	my ($dir, $ref) = @_;
	my ($day, $year, $d);

	($d, $d, $d, $d, $day, $year, $d, $day, $d) = localtime(time);
	$year += 1900;
	open (ENTRIES, "$dir/CVS/Entries");
	foreach (<ENTRIES>) {
		my ($name, $ver, $time);

		if (m,^/(.+)/([\d\.]+)/(.*)/,) {
			my ($fday, $fmonth, $fyear);

			($name, $ver, $time) = ($1, $2, $3);
			$time =~ m/.{4}(.{3}) +(\d{1,2}).{10}(.{4})/;
			($fmonth, $fday, $fyear) = ($1, $2, $3);
			$fday = $months{$fmonth} + $fday;
			if ($year == $fyear) {
				my $diff = $day - $fday;
				if (($day - $fday) < 7) {
					if ($ver =~ /[2-9]/) {
						$$ref{$name} = "UPDATED";
					} else {
						$$ref{$name} = "NEW";
					}
				}
			}
		}
	}
	close (ENTRIES);
}

sub add_to_index {
	my ($path, $page, $title, $dateref) = @_;
	my $url;

	$url = $$dateref{"$page"};
	if ($page =~ m/^http/) {
		$url .= "$path;" unless ($path eq "");
	} else {
		$url .= "$path/" unless ($path eq "");
	}
	$url .= "$page";
	push (@main_index, "$url:$title");
}

sub parse_dir ($$$$) {
	my ($dir, $parentref, $parenttitle, $indexdir) = @_;
	my ($indexlink, $indextitle, $indexpfx);
	my (@children, @parents, $fh, $path);
	my (%dates);

	if ($parentref) {
		@parents = @$parentref;
	}
	$path = $dir;
	$path =~ s/$contentdir//;

	read_dates ($dir, \%dates);
	if ($indexdir) {
		if ($dates{"index.html"}) {
			$indexdir = $dates{"index.html"} . $indexdir;
		}
		push (@main_index, $indexdir);
	}
	if (!(-e "$dir/$treefile")) {
		return;
	}
	$fh = new FileHandle ("$dir/$treefile", "r");
	while ($fh && ($_ = $fh->getline())) {
		my ($page, $mainindexflag, $title, $linktitle);

		chomp;
		die "DIR file $dir/$treefile is corrupted\n"
			unless m/(.+):(.):(.*):(.+)/;
		($page, $mainindexflag, $title, $linktitle) =
			($1, $2, $3, $4);

		if ($page eq "index.html") {
			&add_parent (\@parents, "$path/index.html", $linktitle);
			$indextitle = $title;
			if ($parenttitle ne "") {
				$indexlink = "$parenttitle: " . $linktitle;
			} else {
				$indexlink = $linktitle
			}
			next;
		}
		die "Put index.html at the top\n" unless $indextitle;

		print "processing $path/$page\n";

		if ($page =~ m,http,) {
			&add_child (\@children, $page, $linktitle);
			if ($mainindexflag eq "+") {
				add_to_index ($path, $page, $title, \%dates);
			}
			next;
		}
		# AFTER non-relative urls, because those can end on /
		if ($page =~ m,(.*)/$,) {
			my $dirindex = undef;

			if ($mainindexflag eq "+") {
				$dirindex = "$path/$page:$title";
			}
			&add_child (\@children, $page, $linktitle);
			&parse_dir ("$dir/$1", \@parents, $indexlink, $dirindex);
			next;
		}
		if ($page =~ m,(.*html)\??,) {
			my $localpage = $1;
			&add_child (\@children, $page, $linktitle);
			&fill_page ($path, $localpage, $title,
		            	"$indexlink: $linktitle", \@parents, undef);
			if ($mainindexflag eq "+") {
				add_to_index ($path, $page, $title, \%dates);
			}
			next;
		}
	}
	&fill_page ($path, "index.html", "$indextitle",
	            $indexlink, $parentref, \@children);
	close ($fh);
	return ($dates{"index.html"});
}

sub add_child (\$$) {
	my ($childrenref, $child, $url) = @_;

	push (@$childrenref, "$child:$url");
}

sub add_parent (\$$) {
	my ($parentref, $dir, $url) = @_;
	
	push (@$parentref, "$dir:$url");
}

sub parse_opts () {
	my ($i);

	foreach $i (@ARGV) {
		if      ($i =~ m,--htmldir=(.*)/?,) {
			$htmldir = $1;
		} elsif ($i =~ m,--basedir=(.*)/?,) {
			$basedir = $1;
		} elsif ($i =~ m,--templatepfx=(.*),) {
			$templatepfx = $1;
		}
	}
}

sub load_vars ($) {
	my ($dir) = @_;

	open (VARS, "$dir/VARS");
	while (<VARS>) {
		m/(\w+)=(.*)/;
		$variables{$1} = $2;
	}
	close (VARS);
}

sub main () {
	&parse_opts;
	if ($basedir eq "") {
		$basedir = ".";
	}
	$contentdir = "$basedir/content";
	$templatesdir = "$basedir/templates";
	if ($htmldir eq "") {
		$htmldir = "/tmp/html";
	}
	&load_vars ($basedir);

	&parse_dir ($contentdir, undef, "", undef);
}

&main();
