DROP TABLE applist;
CREATE TABLE applist (
	name VARCHAR(100) NOT NULL PRIMARY KEY,
	version VARCHAR(15),
	maintainer_name VARCHAR(128) NOT NULL,
	maintainer_email VARCHAR(128) NOT NULL,
	description TEXT,
	keywords VARCHAR(255),
	requires VARCHAR(255),
	url_homepage VARCHAR(255),
	url_screenshot VARCHAR(255),
	url_download VARCHAR(255),
	category VARCHAR(255),
	added VARCHAR(30),
	touched TIMESTAMP
);
