include templates/Makefile.mk

SUBDIRS = \
	images	\
	cgi-bin	\
	content

all:
	mkdir -p ${HTMLDIR}
	for i in ${SUBDIRS}; do \
          make -C "$$i"; \
	done
	${PERL} ${TOPDIR}/scripts/buildsite.pl --htmldir=${HTMLDIR} --basedir=${TOPDIR} --templatepfx=${TEMPLATE}

clean:
	for i in ${SUBDIRS}; do \
		make -C "$$i" clean; \
	done
